// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GFDL-1.3-no-invariants-only

/*!
    \module QtInterfaceFramework
    \title Qt Interface Framework C++ Classes
    \ingroup modules
    \qtvariable interfaceframework

    \brief C++ classes for the Qt Interface Framework Core API.

    To link against the Qt Interface Framework module, use the following directive:

    Using CMake:

    \code
    find_package(Qt6 COMPONENTS InterfaceFramework REQUIRED)
    target_link_libraries(mytarget PRIVATE Qt6::InterfaceFramework)
    \endcode

    Using qmake:

    \code
    QT += interfaceframework
    \endcode

    To use Qt Interface Framework C++ classes in your application, use the
    following include statement:

    \code
    #include <QtInterfaceFramework>
    \endcode
*/

/*!
    \qmlmodule QtInterfaceFramework
    \title Qt Interface Framework QML Types
    \ingroup qmlmodules

    \brief QML types for the Qt Interface Framework Core API.

    The Qt Interface Framework Core QML API provides types which are used together with your own
    API.

    To import the QML types into your application, use the following import statement
    in your .qml file:

    \code
    import QtInterfaceFramework
    \endcode

    \section1 QML Types
*/

/*!
    \group qtinterfaceframework-examples
    \ingroup all-examples
    \title Qt Interface Framework Examples

    \brief Examples for the Qt Interface Framework module

    These are the Qt Interface Framework examples.
*/
