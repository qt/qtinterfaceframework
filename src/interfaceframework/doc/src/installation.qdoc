// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GFDL-1.3-no-invariants-only
/*!
\page interfaceframework-installation.html
\title Installation

Since the QtInterfaceFramework module uses the same \l{Qt Configure Options}{Configure System} as the rest of Qt,
you can do build-time configuration and enable only the features that you need.

\section1 Features Available

The following table describes the features available in the module. Similar to other modules, the
available features are auto-detected and displayed after the configuration step in the "Configure
summary".

\table
\header
    \li Feature
    \li Dependency
    \li Description
\row
    \li Interface Framework Generator
        \keyword feature-ifcodegen
    \li python3
        python3-venv
    \li The Interface Framework Generator provides tooling to generate source
        code from Interface Definition Language (IDL) files. The Interface Framework
        Generator comes with a set of templates for specific code generation use cases.
\row
    \li QtRemoteObjects Support
        \keyword feature-qtremoteobjects
    \li QtRemoteObjects module
    \li The QtRemoteObjects Support is needed to generate \c interfaceframework back ends, using QtRemoteObjects
        for its Inter-Process Communication (IPC). This feature also provides templates to
        generate the server part of this IPC.
\endtable

\section1 General Build Process

To build QtInterfaceFramework modules, run the following commands:

\code
mkdir <builddir>
  && cd <builddir>
  && cmake <srcdir> <options>
  && cmake --build .
  && cmake --install .
\endcode
or
\code
mkdir <builddir>
  && cd <builddir>
  && <qtbase>/bin/qt-configure-module <srcdir> <options>
  && cmake --build .
  && cmake --install .
\endcode

*/
